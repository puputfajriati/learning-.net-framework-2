namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_assignto_tasktable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "AssignTo_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Tasks", "AssignTo_Id");
            AddForeignKey("dbo.Tasks", "AssignTo_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "AssignTo_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Tasks", new[] { "AssignTo_Id" });
            DropColumn("dbo.Tasks", "AssignTo_Id");
        }
    }
}
