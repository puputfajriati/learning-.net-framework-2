namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_isdeleted_column_aspnetuser_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsDeleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsDeleted");
        }
    }
}
