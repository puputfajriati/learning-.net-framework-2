namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_task : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TaskName = c.String(),
                        Status = c.Boolean(),
                        CreatedDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        CreatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .Index(t => t.CreatedBy_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "CreatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Tasks", new[] { "CreatedBy_Id" });
            DropTable("dbo.Tasks");
        }
    }
}
