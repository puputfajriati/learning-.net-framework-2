namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changename_assigntoid_and_virtual_applicationuser_createdby_aspnetusertable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tasks", name: "AssignTo_Id_Id", newName: "AssignToId");
            RenameIndex(table: "dbo.Tasks", name: "IX_AssignTo_Id_Id", newName: "IX_AssignToId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Tasks", name: "IX_AssignToId", newName: "IX_AssignTo_Id_Id");
            RenameColumn(table: "dbo.Tasks", name: "AssignToId", newName: "AssignTo_Id_Id");
        }
    }
}
