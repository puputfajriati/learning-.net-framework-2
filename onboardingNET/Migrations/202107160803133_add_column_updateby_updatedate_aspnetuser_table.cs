namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_updateby_updatedate_aspnetuser_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UpdateDate", c => c.String());
            AddColumn("dbo.AspNetUsers", "UpdateBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UpdateBy");
            DropColumn("dbo.AspNetUsers", "UpdateDate");
        }
    }
}
