namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changetypedata_createdby_aspnetusertable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tasks", "CreatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Tasks", new[] { "CreatedBy_Id" });
            AddColumn("dbo.Tasks", "CreatedBy", c => c.String());
            DropColumn("dbo.Tasks", "CreatedBy_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "CreatedBy_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.Tasks", "CreatedBy");
            CreateIndex("dbo.Tasks", "CreatedBy_Id");
            AddForeignKey("dbo.Tasks", "CreatedBy_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
