namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changename_assignto_id_createdby_aspnetusertable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tasks", name: "AssignTo_Id", newName: "AssignTo_Id_Id");
            RenameIndex(table: "dbo.Tasks", name: "IX_AssignTo_Id", newName: "IX_AssignTo_Id_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Tasks", name: "IX_AssignTo_Id_Id", newName: "IX_AssignTo_Id");
            RenameColumn(table: "dbo.Tasks", name: "AssignTo_Id_Id", newName: "AssignTo_Id");
        }
    }
}
