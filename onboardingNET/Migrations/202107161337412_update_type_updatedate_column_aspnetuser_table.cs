namespace onboardingNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_type_updatedate_column_aspnetuser_table : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "UpdateDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "UpdateDate", c => c.String());
        }
    }
}
