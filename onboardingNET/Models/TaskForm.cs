﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace onboardingNET.Models
{
    public class TaskForm
    {
        [Required]
        [Display(Name = "Task Name")]
        public string TaskName { get; set; }
        [Display(Name = "Status Name")]
        public bool? Status { get; set; }
        [Required]
        [Display(Name = "Finish Date")]
        public DateTime? FinishDate { get; set; }
        public string CreatedBy { get; set; }
        [Required]
        [Display(Name = "Assign To")]
        public string selectedValue { get; set; }
        public ApplicationUser AssignTo { get; set; }
    }
}