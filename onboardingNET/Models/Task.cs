﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace onboardingNET.Models
{
    public class Task
    {
        public string Id { get; set; }
        public string TaskName { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public string AssignToId { get; set; }

        [ForeignKey("AssignToId")]
        public virtual ApplicationUser User { get; set; }
        public string CreatedBy { get; set; }
    }
}