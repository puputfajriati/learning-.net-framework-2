﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace onboardingNET.Models
{
    public class TaskViewModel
    {
        public TaskForm Form { set; get; }
        public List<SelectListItem> ListUser { set; get; }
        public DateTime? CreatedDate { get; set; }
        public string TaskId { set; get; }
    }
}