﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace onboardingNET.Models
{
    public class UserForm
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public ApplicationUser UpdateBy { get; set; }

    }
}