﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using onboardingNET.Models;
using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace onboardingNET.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public UsersController()
        {
        }
        public UsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public async Task<ActionResult> Index()
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var userid = prinicpal.Claims.ToList();
            //var userid = prinicpal.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            var users = await UserManager.Users.ToListAsync();
            var userViewModel = new List<ApplicationUser>();
            foreach (ApplicationUser user in users)
            {
                var thisViewModel = new ApplicationUser()
                {
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName,
                    PasswordHash = user.PasswordHash,
                    IsDeleted = user.IsDeleted
                };
                userViewModel.Add(thisViewModel);
            }
            return View(userViewModel);
        }


        // GET: /Users/Register
        public ActionResult Register()
        {
            //var identity = (ClaimsIdentity)User.Identity;
            //IEnumerable<Claim> claims = identity.Claims;
            //var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            //var email = prinicpal.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            //email.Value.ToString();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    return RedirectToAction("Index", "Users");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> EditAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var task = await UserManager.FindByIdAsync(id);
            var usermodel = new UsersViewModel()
            {
                Email = task.Email,
                FirstName = task.FirstName,
                LastName = task.LastName,
                PasswordHash = task.PasswordHash
            };
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(usermodel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateUsers(UsersViewModel model)
        {
            var test = ModelState.IsValid;
            var modl = model;
            if (ModelState.IsValid)
            {
                var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var userid = prinicpal.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
                var user = await UserManager.FindByIdAsync(model.Id);
                var email = model.Email;
                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.PasswordHash);
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                //user.PasswordHash = model.PasswordHash;
                user.UpdateDate = DateTime.Now;
                user.UpdateBy = User.Identity.GetUserId();
                IdentityResult result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    Errors(result);
            }
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> DeleteAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var task = await UserManager.FindByIdAsync(id);
            Users userList = new Users()
            {
                Id = task.Id,
                Email = task.Email,
                FirstName = task.FirstName,
                LastName = task.LastName,
                PasswordHash = task.PasswordHash
            };

            if (task == null)
            {
                return HttpNotFound();
            }
            return View(userList);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUsers(UsersViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            user.UpdateDate = DateTime.Now;
            user.UpdateBy = model.Id;
            user.IsDeleted = true;
            IdentityResult result = await UserManager.UpdateAsync(user);
            if (result.Succeeded)
                return RedirectToAction("Index");
            else
                Errors(result);

            return RedirectToAction("DeleteUsers/", model.Id);
        }

        public async Task<ActionResult> RestoreAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var task = await UserManager.FindByIdAsync(id);
            Users userList = new Users()
            {
                Id = task.Id,
                Email = task.Email,
                FirstName = task.FirstName,
                LastName = task.LastName,
                PasswordHash = task.PasswordHash
            };

            if (task == null)
            {
                return HttpNotFound();
            }
            return View(userList);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RestoreUsers(UsersViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            user.UpdateDate = DateTime.Now;
            user.UpdateBy = model.Id;
            user.IsDeleted = false;
            IdentityResult result = await UserManager.UpdateAsync(user);
            if (result.Succeeded)
                return RedirectToAction("Index");
            else
                Errors(result);

            return RedirectToAction("DeleteUsers/", model.Id);
        }

        private void Errors(IdentityResult result)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ActionResult> SendEmailAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            ViewBag.Message = "Your Email has sent.";

            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}