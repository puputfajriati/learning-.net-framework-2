﻿using onboardingNET.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Threading;
using System.Web.Security;

namespace onboardingNET.Controllers
{
    public class TaskServices : ITask
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        private string userid { set; get; }
        public TaskServices()
        {
        }

        public TaskServices(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public void CreateTaskAsync(string TaskName, DateTime? FinishDate, string selecteduser)
        {
             var uidlogin = HttpContext.Current.User.Identity.GetUserId();               
            //var use = await UserManager.FindByIdAsync(userid);
            var Uid = Guid.NewGuid().ToString();
            Task task = new Task()
            {
                Id = Uid,
                TaskName = TaskName,
                Status = true,
                CreatedDate = DateTime.Now,
                AssignToId = selecteduser,
                CreatedBy = uidlogin,
                FinishDate = FinishDate
            };

            db.Tasks.Add(task);
            db.SaveChanges();

        }

        public List<Task> ListTask
        {
            get
            {
                return db.Tasks.ToList();
            }
        }
        public TaskViewModel ListTaskVModel()
        {
            TaskViewModel vm = new TaskViewModel();
            var users = db.Users.ToList();

            List<SelectListItem> OptionText = new List<SelectListItem>();
            OptionText.Add(new SelectListItem { Text = "Please Select...", Value = "", Selected = true });
            foreach (var i in users)
            {
                SelectListItem li = new SelectListItem
                {
                    Value = i.Id,
                    Text = i.FirstName + ' ' + i.LastName
                };
                OptionText.Add(li);
            }
            vm.ListUser = OptionText;
            return vm;
        }
        public Task ListTaskEdit(string id)
        {
            Task task = db.Tasks.Find(id);
            return task;
        }

        public void EditTask(TaskViewModel task)
        {

            Task tasks = new Task()
            {
                Id = task.TaskId,
                TaskName = task.Form.TaskName,
                Status = task.Form.Status,
                FinishDate = task.Form.FinishDate,
                AssignToId = task.Form.selectedValue,
                CreatedDate= task.CreatedDate
            };
            db.Entry(tasks).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTask(string id)
        {
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
        }

        public TaskViewModel ListTaskVModelEdit(string id)
        {
            TaskViewModel vm = new TaskViewModel();
            var users = db.Users.ToList();
            var taskselected = db.Tasks.Where(c => c.Id == id).FirstOrDefault();
            var taskname = taskselected.TaskName;
            TaskForm form = new TaskForm()
            {
                TaskName = taskselected.TaskName,
                Status = taskselected.Status,
                FinishDate = taskselected.FinishDate,
                selectedValue = taskselected.AssignToId,
                CreatedBy = taskselected.CreatedBy
            };
            vm.Form = form;
            vm.TaskId = id;
            vm.CreatedDate = taskselected.CreatedDate;
            List<SelectListItem> OptionText = new List<SelectListItem>();
            OptionText.Add(new SelectListItem { Text = "Please Select...", Value = "na", Selected = true });
            foreach (var i in users)
            {
                if (taskselected.AssignToId==i.Id)
                {
                    OptionText.Add(new SelectListItem { Text = i.FirstName + ' ' + i.LastName, Value = i.Id, Selected = true });
                }
                else
                {
                    OptionText.Add(new SelectListItem { Text = i.FirstName + ' ' + i.LastName, Value = i.Id});
                }
            }
            vm.ListUser = OptionText;
            return vm;
        }
    }
}