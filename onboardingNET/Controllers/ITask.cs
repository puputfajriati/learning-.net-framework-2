﻿using onboardingNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace onboardingNET.Controllers
{
    interface ITask
    {
        void CreateTaskAsync(string TaskName, DateTime? FinishDate, string selecteduser);
        List<Task> ListTask { get; }
        Task ListTaskEdit(string id);
        TaskViewModel ListTaskVModel();
        TaskViewModel ListTaskVModelEdit(string id);
        void EditTask(TaskViewModel task);
        void DeleteTask(string id);
    }
}
