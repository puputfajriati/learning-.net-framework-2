﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using onboardingNET.Models;
namespace onboardingNET.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        private TaskServices _services;

        public TasksController(ApplicationUserManager userManager, TaskServices services)
        {
            this._userManager = userManager;
            _services = services;
        }
        public TasksController()
        {
            _services = new TaskServices();
        }
        public ApplicationUserManager UserManager { get; private set; }

        // GET: Tasks
        public ActionResult Index()
        {

            return View(_services.ListTask);
        }

        // GET: Tasks/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            var models=_services.ListTaskVModel();
            ViewBag.Users = models.ListUser;
            return View(_services.ListTaskVModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAsync(TaskForm form)
        {
            var fom = form;
            if (ModelState.IsValid)
            {
                var uid = User.Identity.GetUserId();
                _services.CreateTaskAsync(form.TaskName, form.FinishDate, form.selectedValue);
                return RedirectToAction("Index");
            }

            return View(form);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var models = _services.ListTaskVModelEdit(id);
            ViewBag.Users = models.ListUser;
            var task=_services.ListTaskEdit(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(models);

            //return View(task);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TaskViewModel taskView)
        {
            var form = taskView;
            if (ModelState.IsValid)
            {
                _services.EditTask(taskView);
                return RedirectToAction("Index");
            }
            return View(taskView);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var task = _services.ListTaskEdit(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            _services.DeleteTask(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
