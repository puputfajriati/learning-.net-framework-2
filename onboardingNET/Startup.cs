﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(onboardingNET.Startup))]
namespace onboardingNET
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
